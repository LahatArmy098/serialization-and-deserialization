﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle vehicle = new Vehicle() { CarBrand = "Honda", CarColor = "Black" };
            string filePath = "data.save";
            DataSerializer dataSerializer = new DataSerializer();
            Vehicle v = null;

            dataSerializer.BinarySerialize(vehicle, filePath);

            v = dataSerializer.BinaryDeserialize(filePath) as Vehicle;

            Console.WriteLine(v.CarBrand);
            Console.WriteLine(v.CarColor);

            Console.ReadLine();
        }
    }


    [Serializable]
    public class Vehicle
    {
        public string CarBrand { get; set; }
        public string CarColor { get; set; }
    }

    class DataSerializer
    {
        public void BinarySerialize(object data, string filePath)
        {
            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(filePath)) File.Delete(filePath);
            fileStream = File.Create(filePath);
            bf.Serialize(fileStream, data);
            fileStream.Close();
        }

        public object BinaryDeserialize(string filePath)
        {
            object obj = null;

            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(filePath))
            {
                fileStream = File.OpenRead(filePath);
                obj = bf.Deserialize(fileStream);
                fileStream.Close();
            }

            return obj;
        }
    }

}
